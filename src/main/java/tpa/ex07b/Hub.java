package tpa.ex07b;

import java.util.*;

public class Hub implements Receiver {

    private Map<String, Receiver> modules = new HashMap<String, Receiver>();

    @Override
    public void message(String message) {
        String channel = getChannel(message);

        if (getCommand(message).equals("buy")) {
            getModule("ticket").message(channel + ":buy");

        } else if (getCommand(message).equals("bought")) {
            getModule("bill").message(channel + ":bill");

        } else if (getCommand(message).equals("billed")) {
            getModule("smsOut").message("sendSms");
        }
    }

    private String getChannel(String message) {
        return message.split(":")[0];
    }

    private String getCommand(String message) {
        return message.split(":")[1];
    }

    private Receiver getModule(String name) {
        return modules.get(name);
    }

    public void addModule(String name, Receiver module) {
        modules.put(name, module);
    }

}

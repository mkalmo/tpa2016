Ülesanne 10

Builder mustri kasutamine.

Osa 1

  Kirjutge lõpuni klassid PersonUtilTest ja PersonBuilder.

  Testitav klass on PersonUtil meetoditega:

    getOldest(List<Person> persons) - tagastab vanima isiku.
    getPersonsInLegalAge(List<Person> persons) - tagastab isikud kes on 18+ aastat vanad.
    getWomen(List<Person> persons) - tagastab naissoost isikud.
    getPersonsWhoLiveIn(String town, List<Person> persons) - tagastab isikud, kes elavad määratud linnas.

  Testige ka tühja listi ja null-iga.

Osa 2

  Pange tööle testid klassis OrderServiceTest.
  Selleks kirjutage lõpuni klassid OrderServiceBuilder ja OrderBuilder.

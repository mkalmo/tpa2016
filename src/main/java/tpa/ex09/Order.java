package tpa.ex09;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {

    private boolean filled;
    private String number;
    private Date orderDate;
    private List<LineItem> lineItems;

    public Order(String number, Date date) {
        this.number = number;
        this.orderDate = date;

        lineItems = new ArrayList<LineItem>();
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String getNumber() {
        return number;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void addItem(LineItem lineItem) {
        lineItems.add(lineItem);
    }

    public Double getTotal() {
        return lineItems.stream()
            .mapToDouble(lineItem -> lineItem.getPrice() * lineItem.getQuantity())
            .sum();
    }

    @Override
    public String toString() {
        return "Order [filled=" + filled + ", number=" + number
                + ", orderDate=" + orderDate + "]";
    }
}

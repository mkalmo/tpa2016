package tpa.ex09;

import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.NotImplementedException;

@SuppressWarnings("unused")
public class OrderService {

    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders() {
        throw new NotImplementedException();
    }

    public List<Order> getOrdersOver(double amount) {
        throw new NotImplementedException();
    }

    public List<Order> getOrdersSortedByDate() {
        throw new NotImplementedException();
    }

    private Comparator<Order> getOrderDateComparator() {
        return (Order o1, Order o2) ->
            o1.getOrderDate().compareTo(o2.getOrderDate());
    }
}

package tpa.ex02;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack();
        assertThat(stack.getSize(), is(0));
    }

}

class Stack {

    public Integer getSize() {
        return null;
    }

}
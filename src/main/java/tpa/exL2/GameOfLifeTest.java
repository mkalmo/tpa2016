package tpa.exL2;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class GameOfLifeTest {

    @Test
    public void frameKnowsWhichSquersAreAlive() {
        Frame frame = getFrame("--X",
                               "-X-");

        assertThat(frame.isAlive(p(0, 0)), is(false));

        // ...
    }

    private Point p(int x, int y) {
        return new Point(x, y);
    }

    private Frame getFrame(String ... rows) {
        Frame frame = new Frame(rows[0].length(), rows.length);
        for (Point p : frame.getPoints()) {
            String isAlive = String.valueOf(rows[p.y].charAt(p.x));
            if (Frame.ALIVE.equals(isAlive)) {
                frame.markAlive(p);
            }
        }

        return frame;
    }
}

class Frame {

    public final static String ALIVE = "X";
    public final static String DEAD = "-";

    private boolean[][] matrix;

    public Frame(int width, int height) {
    }

    public List<Point> getPoints() {
        List<Point> points = new ArrayList<Point>();
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                points.add(new Point(x, y));
            }
        }

        return points;
    }

    public Integer getWidth() {
        return matrix[0].length;
    }

    public Integer getHeight() {
        return matrix.length;
    }

    @Override
    public String toString() {
        String retval = "";
        for (int y = 0; y < getHeight(); y++) {
            for (int x = 0; x < getWidth(); x++) {
                retval += matrix[y][x] ? ALIVE : DEAD;
            }
            retval += "\n";
        }

        return retval;
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }

    @Override
    public int hashCode() {
        return 1;
    }

    public Integer getNeighbourCount(Point p) {
        return 0;
    }

    public boolean isAlive(Point p) {
        return false;
    }

    public void markAlive(Point point) {
    }
}

class Point {

    public final int x;
    public final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return MessageFormat.format("p({0}, {1})", x, y);
    }

    @Override
    public boolean equals(Object obj) {
        Point other = (Point) obj;
        return other.x == x && other.y == y;
    }

    @Override
    public int hashCode() {
        return 1;
    }
}